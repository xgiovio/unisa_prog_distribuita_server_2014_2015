package rmi_test_codebase_separate;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Giovanni on 08/12/2014.
 */
public interface i  extends Remote {
    public messaggio message() throws RemoteException;
}
