package rmi_test_codebase_separate;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by Giovanni on 08/12/2014.
 */
public class test extends UnicastRemoteObject implements i {
    @Override
    public messaggio message() throws RemoteException{
        return new messaggio_extended();
    }

    public test() throws RemoteException {
    }
}
