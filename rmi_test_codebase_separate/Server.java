package rmi_test_codebase_separate;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

/**
 * Created by Giovanni on 08/12/2014.
 */
public class Server {

    public static void main (String[] args) {

        try {

            System.setSecurityManager(new SecurityManager());
            i a = new test();
            Registry r = LocateRegistry.getRegistry();
            r.rebind("server",a);


        } catch (RemoteException e) {
            e.printStackTrace();
        }


    }


}
